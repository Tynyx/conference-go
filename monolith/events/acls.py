from .keys import *
import json
import requests
from requests.exceptions import RequestException

# response = requests.get('https://api.pexels.com/v1/search?')


# print(response.text)
def get_city_picture(city, state):
    # use the city and state to get the picture
    # create a dictionary for the headers to use in the request

    response = requests.get(
        f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1&page=1",
        headers={"Authorization": PEXELS_API_KEY},
    )
    content = json.loads(response.content)
    picture_url = content["photos"][0]["src"]["medium"]

    return picture_url


# # def get_weather(city, state):
# # use the city and state to get the weather
# # create a dictionary for the headers to use in the request

# response = requests.get(
#     f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&appid={OPEN_WEATHER_API_KEY}"
# )
# # THIS IS GETTING THE GEO CODE TO USE FOR THE REGULAR WEATHER API
# content = json.loads(response.text)
# lat = content[0]["lat"]
# lon = content[0]["lon"]
# # THIS IS GETTING THE WEATHER USING THE GEO CODE
# response = requests.get(
#     f"https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
# )
# content = json.loads(response.text)
# weather_description = content["current"]["weather"][0]["description"]

# # Extract the temp in Kelvin and convert to Fahrenheit
# temp_k = content["current"]["temp"]
# temp_f = (temp_k - 273.15) * 9 / 5 + 32

# return weather_description, temp_f


# i need to get the lat and lon from the city and state
# i need to use geo code to get the lat and lon
# i need to use the lat and lon to get the weather
# i also need to add a description of the weather to the conference model
# def get_weather(city, state):
#     # use the city and state to get the weather
#     # create a dictionary for the headers to use in the request
#     city = city
#     response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?q={city},{state}&appid={'60d79ab66042cabe3ec97fe2720a36ac'}")


def get_weather(city, state):
    try:
        response = requests.get(
            f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&appid={OPEN_WEATHER_API_KEY}"
        )
        response.raise_for_status()  # Raises a HTTPError if the status is 4xx, 5xx
        content = response.json()
        lat = content[0]["lat"]
        lon = content[0]["lon"]

        response = requests.get(
            f"https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
        )
        response.raise_for_status()
        content = response.json()
        weather_description = content["current"]["weather"][0]["description"]
        temp_k = content["current"]["temp"]
        temp_f = (temp_k - 273.15) * 9 / 5 + 32

        return weather_description, temp_f

    except RequestException as e:
        print(f"Request failed: {e}")
        return None, None
    except KeyError:
        print("Invalid response data")
        return None, None
