from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location
from django.views.decorators.http import require_http_methods
import json
from .models import State
from .acls import get_city_picture, get_weather


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(
    [
        "GET",
        "POST",
        "PUT",
    ]
)
def api_list_conferences(request):
    if request.method == "GET":
        """
        Lists the conference names and the link to the conference.

        Returns a dictionary with a single key "conferences" which
        is a list of conference names and URLS. Each entry in the list
        is a dictionary that contains the name of the conference and
        the link to the conference's information.

        {
            "conferences": [
                {
                    "name": conference's name,
                    "href": URL to the conference,
                },
                ...
            ]
        }
        """

        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        # get the location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id."},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {
            "state": o.state.abbreviation,
        }


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationDetailEncoder(),
    }


@require_http_methods(
    [
        "GET",
        "PUT",
        "DELETE",
    ]
)
def api_show_conference(request, id):
    if request.method == "GET":
        """
        Returns the details for the Conference model specified
        by the id parameter.

        This should return a dictionary with the name, starts,
        ends, description, created, updated, max_presentations,
        max_attendees, and a dictionary for the location containing
        its name and href.

        {
            "name": the conference's name,
            "starts": the date/time when the conference starts,
            "ends": the date/time when the conference ends,
            "description": the description of the conference,
            "created": the date/time when the record was created,
            "updated": the date/time when the record was updated,
            "max_presentations": the maximum number of presentations,
            "max_attendees": the maximum number of attendees,
            "location": {
                "name": the name of the location,
                "href": the URL for the location,
            }
            "weather": {
                "temperature": the temperature of the weather,
                "description": the description of the weather,
            }
        }
        """
        conference = Conference.objects.get(id=id)
        weather_description, temp_f = get_weather(conference.location.city, conference.location.state.abbreviation) 
        conference_data = ConferenceDetailEncoder().default(conference) 
        conference_data.update({"weather_description": weather_description, "temp_f": temp_f})
        return JsonResponse(conference_data, safe=False)
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id."},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


@require_http_methods(
    [
        "GET",
        "POST",
    ]
)
def api_list_locations(request):
    if request.method == "GET":
        """
        Lists the location names and the link to the location.

        Returns a dictionary with a single key "locations" which
        is a list of location names and URLS. Each entry in the list
        is a dictionary that contains the name of the location and
        the link to the location's information.

        {
            "locations": [
                {
                    "name": location's name,
                    "href": URL to the location,
                },
                ...
            ]
        }
        """
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation."},
                status=400,
            )
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE", "POST"])
def api_show_location(request, id):
    if request.method == "GET":
        """
        Returns the details for the Location model specified
        by the id parameter.

        This should return a dictionary with the name, city,
        room count, created, updated, and state abbreviation.

        {
            "name": location's name,
            "city": location's city,
            "room_count": the number of rooms available,
            "created": the date/time when the record was created,
            "updated": the date/time when the record was updated,
            "state": the two-letter abbreviation for the state,
            "picture_url": the URL for the picture of the location,
        }
        """
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        picture_url = get_city_picture(content["city"], content["state"])
        content.update({"picture_url": picture_url})
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation."},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
